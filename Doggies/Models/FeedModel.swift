//
//  FeedModel.swift
//  Doggies
//
//  Created by Polina Prokopenko on 10/11/21.
//

import Foundation
import RxDataSources

struct FeedImage: Equatable, Codable {
    
    let id: String
    let url: String
    
}

extension FeedImage: IdentifiableType {
    
    typealias Identity = String
    
    var identity: Identity {
        return id
    }
    
}

struct SectionOfFeed {
    
    var header: String = ""
    var items: [Item]
    
    var identity: String {
        return header
    }
    
}

extension SectionOfFeed: AnimatableSectionModelType {
    
    typealias Item = FeedImage
    
    init(original: SectionOfFeed, items: [Item]) {
        self = original
        self.items = items
    }
    
}
