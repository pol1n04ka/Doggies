//
//  BreedModel.swift
//  Doggies
//
//  Created by Polina Prokopenko on 10/20/21.
//

import Foundation

struct Breed: Codable {
    var weight:             BreedWeight
    var height:             BreedHeight
    var id:                 Int
    var name:               String
    var bredFor:            String?
    var breedGroup:         String?
    var lifeSpan:           String
    var temperament:        String?
    var origin:             String?
    var referenceImageId:   String
    var image:              BreedImage
}

struct BreedImage: Codable {
    var height: Int
    var width:  Int
    var id:     String
    var url:    String
}

struct BreedHeight: Codable {
    var imperial: String
    var metric:   String
}

struct BreedWeight: Codable {
    var imperial: String
    var metric:   String
}
