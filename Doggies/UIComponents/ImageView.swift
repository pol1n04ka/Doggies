//
//  ImageView.swift
//  Doggies
//
//  Created by Polina Prokopenko on 10/25/21.
//

import UIKit

class CustomImageView: UIImageView {
    
    enum CustomImageViewStyle {
        case common
        case roundedCorners
    }
    
    init(style: CustomImageViewStyle) {
        super.init(frame: .zero)
        
        clipsToBounds = true
        contentMode   = .scaleAspectFill
        
        translatesAutoresizingMaskIntoConstraints = false
        
        switch style {
        case .common:
            break
        case .roundedCorners:
            layer.masksToBounds = true
            layer.cornerRadius = 15
            backgroundColor = .systemGray
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
