//
//  CollectionView.swift
//  Doggies
//
//  Created by Polina Prokopenko on 10/25/21.
//

import UIKit

class CustomCollectionView: UICollectionView {
    
    init(delegate: UICollectionViewDelegateFlowLayout) {
        super.init(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        
        self.delegate   = delegate
        backgroundColor = .systemGroupedBackground
        
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
