//
//  TabBar.swift
//  Doggies
//
//  Created by Polina Prokopenko on 10/10/21.
//

import UIKit

class TabBar: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    func setup() {
        view.backgroundColor = .systemGroupedBackground
        view.translatesAutoresizingMaskIntoConstraints = false
        
        let feedView = makeNavigationController(vc: FeedView(), title: "Feed", image: UIImage(systemName: "list.bullet.rectangle")!)

        let breedsView = makeNavigationController(vc: BreedsView(), title: "Breeds", image: UIImage(systemName: "book")!)
        
        let favouritesView = makeNavigationController(vc: FavouritesView(), title: "Favourites", image: UIImage(systemName: "star")!)
        
        let controllers = [
            feedView,
            breedsView,
            favouritesView
        ]
        
        viewControllers = controllers
    }
    
    func makeNavigationController(vc: UIViewController, title: String, image: UIImage) -> UINavigationController {
        vc.title = title
        vc.tabBarItem.image = image
        
        let navigationVC = UINavigationController(rootViewController: vc)
        navigationVC.navigationBar.prefersLargeTitles = true
        navigationVC.navigationBar.sizeToFit()
        
        return navigationVC
    }
    
}
