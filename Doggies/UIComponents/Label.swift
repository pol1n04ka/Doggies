//
//  Label.swift
//  Doggies
//
//  Created by Polina Prokopenko on 10/25/21.
//

import UIKit

class CustomLabel: UILabel {
    
    enum CustomLabelStyle {
        case heading
        case body
        case secondGray
    }
    
    init(style: CustomLabelStyle) {
        super.init(frame: .zero)
        
        sizeToFit()
        numberOfLines = 0
        translatesAutoresizingMaskIntoConstraints = false
        
        switch style {
        case .heading:
            font = UIFont(name: "HelveticaNeue-Bold", size: 25)
        case .body:
            lineBreakMode = .byWordWrapping
        case .secondGray:
            textColor     = .systemGray
            lineBreakMode = .byWordWrapping
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
