//
//  Layout.swift
//  Doggies
//
//  Created by Polina Prokopenko on 10/15/21.
//

import UIKit

// MARK: Layout for all collection views
class LayoutDelegate: NSObject, UICollectionViewDelegateFlowLayout {
    
    // MARK: Values for layout
    let screen = UIScreen.main.bounds
    let sectionInsets = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    var itemsPerRow: CGFloat = 2
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = screen.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return sectionInsets.left
    }
    
}
