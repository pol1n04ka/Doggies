//
//  FeedView.swift
//  Doggies
//
//  Created by Polina Prokopenko on 10/21/21.
//

import UIKit
import RxSwift
import RxCocoa

class FeedView: UIViewController {
    
    let layoutDelegate = LayoutDelegate()
    let viewModel      = FeedViewModel()
    
    lazy var collectionView = CustomCollectionView(delegate: layoutDelegate)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        viewModel.setupCollectionView(collectionView)
        viewModel.fetchImages()
    }
    
}

// MARK: Setup view
extension FeedView {
    
    func setupView() {
        view.addSubview(collectionView)
        
        let constraints = [
            collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
}
