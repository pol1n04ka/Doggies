//
//  FeedCell.swift
//  Doggies
//
//  Created by Polina Prokopenko on 10/22/21.
//

import UIKit
import RxSwift
import RxRelay

class FeedCell: UICollectionViewCell {
    
    static let Identifier = "FeedCell"
    var imageUrl = BehaviorRelay<FeedImage?>(value: nil)
    
    lazy var photo = CustomImageView(style: .roundedCorners)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        loadImageWhenAvailable()
    }
    
    func loadImageWhenAvailable() {
        imageUrl
            .subscribe { event in
                guard let imageContainer = event.element else { return }
                guard let image = imageContainer?.url else { return }
                self.photo.fetchImage(from: image)
            }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        photo.image = nil
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension FeedCell {
    
    func setupView() {
        addSubview(photo)
        
        let constraints = [
            photo.topAnchor.constraint(equalTo: topAnchor),
            photo.leadingAnchor.constraint(equalTo: leadingAnchor),
            photo.trailingAnchor.constraint(equalTo: trailingAnchor),
            photo.bottomAnchor.constraint(equalTo: bottomAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
}
