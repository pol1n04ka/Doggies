//
//  FeedViewModel.swift
//  Doggies
//
//  Created by Polina Prokopenko on 10/21/21.
//

import Foundation
import RxSwift
import RxCocoa
import RxDataSources

class FeedViewModel {
    
    // FIXME: Move images urls from feed api on refactor
    private let disposeBag = DisposeBag()
    private let api = FeedAPI()
    private var dataSource: RxCollectionViewSectionedReloadDataSource<SectionOfFeed>!
    
    func fetchImages() {
        api.fetchImagesUrls()
    }
    
    func setupCollectionView(_ collectionView: UICollectionView) {
        collectionView.register(FeedCell.self, forCellWithReuseIdentifier: FeedCell.Identifier)
        
        dataSource = RxCollectionViewSectionedReloadDataSource<SectionOfFeed>(
            configureCell: { datasource, collectionView, indexPath, item in
                if indexPath.row == self.api.feedImagesUrls.value.count - 1 {
                    self.api.fetchImagesUrls()
                }
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FeedCell.Identifier, for: indexPath) as! FeedCell
                
                cell.imageUrl.accept(item)
                
                return cell
            }
        )
        
        api.feedImagesUrls
            .map { images in
                [SectionOfFeed(items: images)]
            }
            .bind(to: collectionView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
    }
    
}
