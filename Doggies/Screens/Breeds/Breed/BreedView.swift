//
//  BreedView.swift
//  Doggies
//
//  Created by Polina Prokopenko on 10/21/21.
//

import UIKit

class BreedView: UIViewController {
    
    // MARK: Wrappers for scrollable content
    let scrollView  = UIScrollView()
    let contentView = UIView()
    
    // MARK: UI elements
    lazy var photo       = CustomImageView(style: .common)
    lazy var name        = CustomLabel(style: .heading)
    lazy var group       = CustomLabel(style: .body)
    lazy var origin      = CustomLabel(style: .body)
    lazy var breedFor    = CustomLabel(style: .body)
    lazy var temperament = CustomLabel(style: .body)
    lazy var lifeSpan    = CustomLabel(style: .body)
    lazy var height      = CustomLabel(style: .body)
    lazy var weight      = CustomLabel(style: .body)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupScrollView()
        setupViews()
    }
    
}

// MARK: Method for load data before presenting
extension BreedView {
    
    func prepareForPresent(photo: String,
                           name: String,
                           group: String,
                           origin: String,
                           breedFor: String,
                           temperament: String,
                           lifeSpan: String,
                           height: String,
                           weight: String) {
        self.photo.fetchImage(from: photo)
        self.name.text        = name
        self.group.text       = group
        self.origin.text      = origin
        self.breedFor.text    = breedFor
        self.temperament.text = temperament
        self.lifeSpan.text    = lifeSpan
        self.height.text      = height
        self.weight.text      = weight
    }
    
}


// MARK: Setup views
extension BreedView {
    
    func setupViews() {
        contentView.addSubview(photo)
        contentView.addSubview(name)
        contentView.addSubview(group)
        contentView.addSubview(origin)
        contentView.addSubview(breedFor)
        contentView.addSubview(temperament)
        contentView.addSubview(lifeSpan)
        contentView.addSubview(height)
        contentView.addSubview(weight)
        
        let constraints = [
            
            photo.topAnchor.constraint(equalTo: contentView.topAnchor),
            photo.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            photo.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            photo.heightAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1),
            
            name.topAnchor.constraint(equalTo: photo.bottomAnchor, constant: 15),
            name.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            name.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),

            group.topAnchor.constraint(equalTo: name.bottomAnchor, constant: 10),
            group.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            group.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),

            origin.topAnchor.constraint(equalTo: group.bottomAnchor, constant: 10),
            origin.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            origin.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            
            breedFor.topAnchor.constraint(equalTo: origin.bottomAnchor, constant: 10),
            breedFor.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            breedFor.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),

            temperament.topAnchor.constraint(equalTo: breedFor.bottomAnchor, constant: 10),
            temperament.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            temperament.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            
            lifeSpan.topAnchor.constraint(equalTo: temperament.bottomAnchor, constant: 10),
            lifeSpan.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            lifeSpan.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            
            height.topAnchor.constraint(equalTo: lifeSpan.bottomAnchor, constant: 10),
            height.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            height.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            
            weight.topAnchor.constraint(equalTo: height.bottomAnchor, constant: 10),
            weight.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            weight.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            weight.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -20)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
}


// MARK: Setup scroll view
extension BreedView {
    
    func setupScrollView() {
        view.backgroundColor = .systemGroupedBackground
        
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        contentView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
                
        let constaints = [
            scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            scrollView.widthAnchor.constraint(equalTo: view.widthAnchor),
            scrollView.topAnchor.constraint(equalTo: view.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            
            contentView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
            contentView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor)
        ]
        
        NSLayoutConstraint.activate(constaints)
    }
    
}
