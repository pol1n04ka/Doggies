//
//  BreedsView.swift
//  Doggies
//
//  Created by Polina Prokopenko on 10/21/21.
//

import UIKit
import RxSwift
import RxCocoa


class BreedsView: UIViewController {
    
    let viewModel = BreedsViewModel(breeds: [])
    
    lazy var tableView: UITableView = {
        let table = UITableView(frame: .zero, style: .insetGrouped)
        
        table.rowHeight = UITableView.automaticDimension
        table.translatesAutoresizingMaskIntoConstraints = false
        
        return table
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        viewModel.bindToTableView(tableView: tableView)
        viewModel.fetchBreeds()
        viewModel.prepareForPresentBreedView(tableView: tableView,
                                             view: self,
                                             viewToPresent: BreedView())
    }
    
}


// MARK: Setup table view
extension BreedsView {
    
    func setupView() {
        view.addSubview(tableView)
        
        let constraints = [
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
}
