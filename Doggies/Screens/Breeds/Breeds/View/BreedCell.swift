//
//  BreedCell.swift
//  Doggies
//
//  Created by Polina Prokopenko on 10/21/21.
//

import UIKit

class BreedCell: UITableViewCell {
    
    var name  = CustomLabel(style: .body)
    var group = CustomLabel(style: .secondGray)
    
    // MARK: Prepare cell
    func setup(data: Breed) {
        self.name.text = data.name
        
        if let group = data.breedGroup {
            if group.isEmpty {
                self.group.text = "Unknown"
            } else {
                self.group.text = group
            }
        } else {
            self.group.text = "Unknown"
        }
    }
    
    // MARK: Cell initializer
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


//MARK: Setup view
extension BreedCell {
    
    func setupView() {
        addSubview(name)
        addSubview(group)
        
        let contraints = [
            name.leadingAnchor.constraint(equalTo: layoutMarginsGuide.leadingAnchor, constant: 10),
            name.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor, constant: 5),
            name.trailingAnchor.constraint(equalTo: layoutMarginsGuide.trailingAnchor, constant: -10),
            name.bottomAnchor.constraint(equalTo: group.topAnchor, constant: -5),
            
            group.leadingAnchor.constraint(equalTo: layoutMarginsGuide.leadingAnchor, constant: 10),
            group.trailingAnchor.constraint(equalTo: layoutMarginsGuide.trailingAnchor, constant: -10),
            group.bottomAnchor.constraint(equalTo: layoutMarginsGuide.bottomAnchor, constant: -5)
        ]
        
        NSLayoutConstraint.activate(contraints)
    }
    
}
