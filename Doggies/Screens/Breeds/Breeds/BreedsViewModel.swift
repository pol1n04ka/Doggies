//
//  BreedsViewModel.swift
//  Doggies
//
//  Created by Polina Prokopenko on 10/21/21.
//

import RxSwift

class BreedsViewModel {
    
    var breedsData: [Breed] = []
    var breeds = PublishSubject<[Breed]>()
    
    // MARK: Get breeds data from api
    // FIXME: Rewrite after rewriting parent function to rx
    func fetchBreeds() {
        BreedsAPI().fetchBreeds { breeds in
            self.breeds.onNext(breeds)
            self.breedsData = breeds
        }
    }
    
    // MARK: Binding breeds data to table view
    func bindToTableView(tableView: UITableView) {
        breeds
            .bind(to: tableView.rx.items) { (tableView: UITableView, index: Int, element: Breed) in
                
                let cell = BreedCell(style: .default, reuseIdentifier: "BreedCell")
                
                cell.setup(data: element)
                
                return cell
            }
    }
    
    // MARK: Prepare breed view for present
    func prepareForPresentBreedView(tableView: UITableView,
                          view: BreedsView,
                          viewToPresent: BreedView) {
        tableView
            .rx
            .itemSelected
            .subscribe(onNext: { indexPath in
                print("Index path is: \(indexPath.row)")
                let breed = self.breedsData[indexPath.row]
                let optionals = self.checkBreedOptionals(breed: breed)
                
                viewToPresent.prepareForPresent(photo: "\(breed.image.url)",
                                                name: "\(breed.name)",
                                                group: "Group: \(optionals["group"]!)",
                                                origin: "Origin: \(optionals["origin"]!)",
                                                breedFor: "Breed for: \(optionals["breedFor"]!)",
                                                temperament: "Temperament: \(optionals["temperament"]!)",
                                                lifeSpan: "Life span: \(breed.lifeSpan)",
                                                height: "Height: \(breed.height.metric) cm",
                                                weight: "Weight: \(breed.weight.metric) kg")
                
                view.present(viewToPresent, animated: true, completion: nil)
                tableView.deselectRow(at: indexPath, animated: true)
            })
    }
    
    // MARK: Check breed values for empty or nil
    // FIXME: Maybe I can refactor it more...
    func checkBreedOptionals(breed: Breed) -> [String : String] {
        var optionalsDictionary: [String : String] = [:]
        
        if let breedGroup = breed.breedGroup {
            if breedGroup.isEmpty {
                optionalsDictionary.updateValue("unknown", forKey: "group")
            } else  {
                optionalsDictionary.updateValue(breedGroup, forKey: "group")
            }
        } else {
            optionalsDictionary.updateValue("unknown", forKey: "group")
        }
        
        if let breedOrigin = breed.origin {
            if breedOrigin.isEmpty {
                optionalsDictionary.updateValue("unknown", forKey: "origin")
            } else  {
                optionalsDictionary.updateValue(breedOrigin, forKey: "origin")
            }
        } else {
            optionalsDictionary.updateValue("unknown", forKey: "origin")
        }
        
        if let bredFor = breed.bredFor {
            if bredFor.isEmpty {
                optionalsDictionary.updateValue("unknown", forKey: "breedFor")
            } else  {
                optionalsDictionary.updateValue(bredFor, forKey: "breedFor")
            }
        } else {
            optionalsDictionary.updateValue("unknown", forKey: "breedFor")
        }
        
        if let breedTemperament = breed.temperament {
            if breedTemperament.isEmpty {
                optionalsDictionary.updateValue("unknown", forKey: "temperament")
            } else  {
                optionalsDictionary.updateValue(breedTemperament, forKey: "temperament")
            }
        } else {
            optionalsDictionary.updateValue("unknown", forKey: "temperament")
        }
        
        return optionalsDictionary
    }
    
    // MARK: Init 
    init(breeds: [Breed]) {
        self.breeds.onNext(breeds)
    }
    
}
