//
//  FeedAPI.swift
//  Doggies
//
//  Created by Polina Prokopenko on 10/22/21.
//

import Foundation
import RxSwift
import RxAlamofire
import RxRelay

// FIXME: Need to refactor
class FeedAPI {
    
    private let disposeBag = DisposeBag()
    
    private let baseURL = "https://api.TheDogAPI.com/v1/images/search"
    private let parameters = "?page=1&limit=20&order=RAND&size=small&mime_types=jpg,png"
    
    let feedImagesUrls = BehaviorRelay<[FeedImage]>(value: [])
    let error = BehaviorRelay<Error?>(value: nil)
    
    func fetchImagesUrls() {
        
        let url = URL(string: "\(baseURL)\(parameters)")
        var request = URLRequest(url: url!)
        request.method = .get
        request.headers = [
            "x-api-key": Constants().apiKey,
            "Accept" : "application/json",
        ]
        
        RxAlamofire
            .requestJSON(request)
            .subscribe { (response, any) in
                if 200..<300 ~= response.statusCode {
                    do {
                        let data = try JSONSerialization.data(withJSONObject: any)
                        let json = try JSONDecoder().decode([FeedImage].self, from: data)
                        self.feedImagesUrls.accept(self.feedImagesUrls.value + json)
                    } catch let error {
                        self.error.accept(error)
                    }
                }
            }.disposed(by: disposeBag)
    }
    
}
