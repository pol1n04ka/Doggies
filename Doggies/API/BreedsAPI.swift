//
//  BreedsAPI.swift
//  Doggies
//
//  Created by Polina Prokopenko on 10/21/21.
//

import Foundation
import RxSwift

// FIXME: Write method for fetch breeds with RxAlamofire

class BreedsAPI {
    
    func fetchBreeds(completion: @escaping ([Breed]) -> Void) {
        let rxRequest = URLSession.shared.rx.data(request: URLRequest(url: URL(string: "https://api.thedogapi.com/v1/breeds?api_key=4763a209-4326-43c4-a01e-faab87da5060")!))
            .subscribe(on: ConcurrentDispatchQueueScheduler(qos: .background))
        
        _ = rxRequest
            .observe(on: MainScheduler.instance)
            .subscribe { event in
                if event.error == nil {
                    guard let data = event.element else { return }
                    
                    var json = [Breed]()
                    
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    
                    do {
                        json = try! decoder.decode([Breed].self, from: data)
                        completion(json)
                    }
                } else {
                    print(event.error!)
                }
            }
    }
    
}
