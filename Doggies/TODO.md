#  Todo

## For all 
- Refactor code for monolith codestyle

### For Feed
- Add haptic touch gesture for collection view with options "Add to favourites", "Download" and "Add to Photos"
- Add screen for view photo 

### For Breeds
- Add search bar 
- Add haptic touch gesture for add breed to favourites

### For Favourites 
- Add segmented view with two tabs: 
    - Photos
    - Breeds
- Add collection view for photos 
- Add table view for breeds

### UIImageView extension 
- rewrite to RxAlamofire

### Optional 
Maybe I will add settings tab
