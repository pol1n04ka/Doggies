//
//  ImageVIewExtension.swift
//  Doggies
//
//  Created by Polina Prokopenko on 10/11/21.
//

import Foundation
import UIKit
import RxSwift

// MARK: Rewrite it to RxAlamofire

let imageCache = NSCache<NSString, UIImage>()

extension UIImageView {
    
    func fetchImage(from url: String) {
        
        let urlForCache = url as NSString
        
        guard let resultUrl = URL(string: url) else { return }
        
        if let imageFromCache = imageCache.object(forKey: urlForCache) {
            
            self.image = imageFromCache
        }
        
        let rxRequest = URLSession.shared.rx.data(request: URLRequest(url: resultUrl))
            .observe(on: ConcurrentDispatchQueueScheduler(qos: .background))
        
        _ = rxRequest
            .observe(on: MainScheduler.instance)
            .subscribe { event in
                
                if event.error == nil {
                    guard let data = event.element else { return }
                    
                    guard let imageToCache = UIImage(data: data) else { return }
                    imageCache.setObject(imageToCache, forKey: urlForCache)
                    
                    DispatchQueue.main.async {
                        self.image = imageToCache
                    }
                } else {
                    print(event.error!)
                }
            }
    }
    
}
